import Vue from 'vue';
import App from './App.vue';
import i18n from './plugins/i18n';
import 'swiper/css/swiper.css';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";


Vue.config.productionTip = false;

Vue.component("v-select", vSelect);

Vue.use(VueAwesomeSwiper);

new Vue({
  render: h => h(App),
  i18n,
}).$mount('#app');
